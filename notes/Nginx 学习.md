# Nginx 学习

## 1. Nginx安装

1. 下载并解压nginx包



    下载并解压nginx包

2. 安装命令

   ```
   1. ./configure #若失败，根据提示，安装相关依赖
   2. make && make install
   ```

3. 启动停止命令

   ```
   #启动
   /usr/local/nginx/sbin/nginx
   #停止
   /usr/local/nginx/sbin/nginx -s stop
   ```

4. 设置Nginx为systemctl方式启动、停止和重启

   - `vim /usr/lib/systemd/system/nginx.service` 创建文件，文件内容如下：

   ```
   [Unit]
   Description=nginx - high performance web server
   After=network.target remote-fs.target nss-lookup.target
   
   [Service]
   Type=forking
   ExecStart=/usr/local/nginx/sbin/nginx
   ExecReload=/usr/local/nginx/sbin/nginx -s reload
   ExecStop=/usr/local/nginx/sbin/nginx -s stop
   
   [Install]
   WantedBy=multi-user.target
   ```

   ```
   [Unit]:服务的说明
   Description:描述服务
   After:描述服务类别
   
   [Service]服务运行参数的设置
   Type=forking是后台运行的形式
   ExecStart为服务的具体运行命令
   ExecReload为重启命令
   ExecStop为停止命令
   PrivateTmp=True表示给服务分配独立的临时空间
   注意：启动、重启、停止命令全部要求使用绝对路径
   
   [Install]服务安装的相关设置，可设置为多用户
   ```

   - systemctl daemon-reload` 使文件生效

   - systemctrl相关命令集合

     ```
     systemctl is-enabled servicename.service #查询服务是否开机启动
     systemctl enable *.service #开机运行服务
     systemctl disable *.service #取消开机运行
     systemctl start *.service #启动服务
     systemctl stop *.service #停止服务
     systemctl restart *.service #重启服务
     systemctl reload *.service #重新加载服务配置文件
     systemctl status *.service #查询服务运行状态
     systemctl --failed #显示启动失败的服务
     ```


## 2. Nginx指令上下文

1. main

   nginx在运行时与具体业务功能（比如http服务或者email服务代理）无关的一些参数，比如工作进程数，运行的身份等。

2. http

   与提供http服务相关的一些配置参数。例如：是否使用keepalive啊，是否使用gzip进行压缩等。

3. server

   http服务上支持若干虚拟主机。每个虚拟主机一个对应的server配置项，配置项里面包含该虚拟主机相关的配置。在提供mail服务的代理时，也可以建立若干server.每个server通过监听的地址来区分。

4. location

   http服务中，某些特定的URL对应的一系列配置项。

   location 匹配规则：

   1、首先查看精准匹配是否匹配成功，如果成功则返回结果并停止解析过程

   2、查看普通匹配是否匹配成功，如果匹配成功，把匹配成功最长的记录下来

   3、依次寻找正则匹配，如果有一个匹配，立马返回结果并停止解析过程

   4、如果正则都不匹配则返回普通匹配中记录的最长匹配

5. mail

   实现email相关的SMTP/IMAP/POP3代理时，共享的一些配置项（因为可能实现多个代理，工作在多个监听地址上）。

配置文件：

```
#nginx用户和组，windows版本不可以指定，例如：user nginx nginx
#user  nobody;

#全局区
#有1个工作的子进程,可以自行修改,但太大无益,因为要争夺CPU,一般设置为 CPU数*核数
worker_processes  1;    
 
#错误日志存放路径
#error_log  logs/error.log;    
#error_log  logs/error.log  notice;
#error_log  logs/error.log  info;
 
#pid（进程标识符）：存放路径。
#pid        logs/nginx.pid; 
 
#使用epoll的I/O 模型。linux建议epoll，FreeBSD建议采用kqueue，window下不指定 
events {
    use epoll; 
 
    #一般是配置nginx连接的特性
    #这是指 一个子进程最大允许连1024个连接
    worker_connections  1024;  
    #keepalive超时时间 
    keepalive_timeout 60;     
 
} 
#这是配置http服务器的主要段
http {
    #设定mime类型,类型由mime.type文件定义
    include       mime.types;       
    default_type  application/octet-stream;
 
 
    #该段为自定义log输出样式函数。配合下面的access_log使用
    #日志格式设置：
    #$remote_addr与$http_x_forwarded_for用以记录客户端的ip地址；
    #$remote_user：用来记录客户端用户名称；
    #$time_local： 用来记录访问时间与时区；
    #$request： 用来记录请求的url与http协议；
    #$status： 用来记录请求状态；成功是200，
    #$body_bytes_sent ：记录发送给客户端文件主体内容大小；
    #$http_referer：用来记录从那个页面链接访问过来的；
    #$http_user_agent：记录客户浏览器的相关信息
    #log_format  main  '$remote_addr - $remote_user [$time_local] "$request" '
    #                  '$status $body_bytes_sent "$http_referer" '
    #                  '"$http_user_agent" "$http_x_forwarded_for"';
    
 
 
    #开启log输出。输出位置为logs/access.log（相对于nginx安装根路径）  使用自定的哪个输出格式
    #access_log  logs/access.log  main;
 
    sendfile        on;
    #tcp_nopush     on;
 
    #keepalive_timeout  0;
    keepalive_timeout  65;
    #是否开启压缩
    #gzip  on;
 
    #这是虚拟主机段
    server {
        #监控端口
        listen       80;
        #监控域名
        server_name  localhost;
 
        #charset koi8-r;
 
        #access_log  logs/host.access.log  main;
 
        #定位，个人理解就是java中的filter
        location / {
            符合条件请求转发路径
            root   html;   
            index  index.html index.htm;
        }
        #404.html;
        #error_page  404             
 
        # redirect server error pages to the static page /50x.html
        #错误码值和对应请求
        error_page   500 502 503 504  /50x.html;        
        location = /50x.html {
            root   html;
        }
    } 
}

```



## 3. 基本应用

### 3.1 虚拟主机配置

```
 server {
        listen 8080; #端口
        server_name 192.168.1.204; #域名
 
        location / {
                root /var/www/html; #根目录
                index index.html;
        access_log  logs/proxy.access.log  main;
        }
    }

```

### 3.2 日志管理

```
log_format log 格式   '配置规则';

日志格式设置：

$remote_addr与$http_x_forwarded_for用以记录客户端的ip地址；

$remote_user：用来记录客户端用户名称；

$time_local： 用来记录访问时间与时区；

$request： 用来记录请求的url与http协议；

$status： 用来记录请求状态；成功是200，

$body_bytes_sent ：记录发送给客户端文件主体内容大小；

$http_referer：用来记录从那个页面链接访问过来的；

$http_user_agent：记录客户浏览器的相关信息

$remote_user：用来记录客户端用户名称；

$time_local： 用来记录访问时间与时区；

$request： 用来记录请求的url与http协议；

$status： 用来记录请求状态；成功是200，

$body_bytes_sent ：记录发送给客户端文件主体内容大小；

$http_referer：用来记录从那个页面链接访问过来的；

$http_user_agent：记录客户浏览器的相关信息
```

### 3.3 反向代理

```
server {
        listen       80;
        server_name  localhost;
        location / {
            #配置代理路径
            proxy_pass http://localhost:8080; 
        }
}
```

### 3.4 负载均衡

网络负载均衡的大致原理是利用一定的分配策略将网络负载平衡地分摊到网络集群的各个操作单元上，使得单个重
负载任务能够分担到多个单元上并行处理，使得大量并发访问或数据流量分担到多个单元上分别处理，从而减少用
户的等待响应时间。Nginx负载均衡使用的是upstream模块。

基本配置如下：

```
upstream tomcat{
        server 192.168.25.134:8080;
        server 192.168.25.134:8081;

    }


    server {
        listen 80;
        server_name localhost;

        location / {
            proxy_pass http://tomcat;
            proxy_set_header Host $host;
            proxy_set_header X-Real-IP $remote_addr;
            proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
            proxy_next_upstream error timeout http_500 http_503;
            proxy_connect_timeout 60s;
            proxy_send_timeout 60s;
            proxy_read_timeout 60s;
            add_header 'Access-Control-Allow-Origin' '*';
            add_header 'Access-Control-Allow-Methods' 'GET,POST,DELETE';
            add_header 'Aceess-Control-Allow-Header' 'Content-Type,*';
        }


    }

```

负载均衡分配方式

1. 轮询（默认）

   每个请求按时间顺序逐一分配到不同的后端服务器，如果后端服务器down掉，能自动剔除

2. weight

   指定轮询几率，weight和访问比率成正比，用于后端服务器性能不均的情况。

   ```
   upstream tomcat{
           server 192.168.25.134:8080 weight=10;
           server 192.168.25.134:8081 weight=3;
       }
   ```

3. 每个请求按访问ip的hash结果分配，这样每个访客固定访问一个后端服务器，可以解决session的问题。

   当我们的程序不是无状态的时候（采用了session保存数据），这时候就有一个很大的很问题了，比如把登录信息保存到了session中，那么跳转到另外一台服务器的时候就需要重新登录了，所以很多时候我们需要一个客户只访问一个服务器，那么就需要用iphash了，iphash的每个请求按访问ip的hash结果分配，这样每个访客固定访问一个后端服务器，可以解决session的问题。

   ```
   upstream tomcat{
           ip_hash;
           server 192.168.25.134:8080;
           server 192.168.25.134:8081;
       }
   ```

4. fair

   需要安装第三方的fair插件才能配置使用。
   按后端服务器的响应时间来分配请求，响应时间短的优先分配。

   ```
   upstream backend { 
           fair; 
           server 192.168.25.134:8080;
           server 192.168.25.134:8081 back;设置为备用主机
       } 
   ```

5. url_hash

   需要安装第三方的url_hash插件才能配置使用。
   按访问url的hash结果来分配请求，使每个url定向到同一个后端服务器，后端服务器为缓存时比较有效。 在upstream中加入hash语句，server语句中不能写入weight等其他的参数，hash_method是使用的hash算法。

   ```
   upstream backend { 
           hash $request_uri; 
           hash_method crc32; 
           server 192.168.25.134:8080;
           server 192.168.25.134:8081;
       } 
   ```

6. 其他配置参数

   1. proxy_next_upstream

      语法：proxy_next_upstream [error | timeout | invalid_header | http_500 | http_502 | http_503 | http_504 | http_404 | off ];
      默认：proxy_next_upstream error timeout;
      配置块：http、server、location
      这个配置表示当向一台上有服务器转发请求出现错误的时候，继续换一台上后服务器来处理这个请求。
      默认情况下，上游服务器一旦开始发送响应数据，Nginx反向代理服务器会立刻把应答包转发给客户端。因此，一
      旦Nginx开始向客户端发送响应包，如果中途出现错误也不允许切换到下一个上有服务器继续处理的。这样做的目
      的是保证客户端只收到来自同一个上游服务器的应答。

   2. proxy_connect_timeout

      语法: proxy_connect_timeout time;
      默认: proxy_connect_timeout 60s;
      范围: http, server, location
      用于设置nginx与upstream server的连接超时时间，比如我们直接在location中设置proxy_connect_timeout 1ms， 1ms很短，如果无法在指定时间建立连接，就会报错。

   3. proxy_send_timeout

      向后端写数据的超时时间，两次写操作的时间间隔如果大于这个值，也就是过了指定时间后端还没有收到数据，连接会被关闭。

   4. proxy_read_timeout

      从后端读取数据的超时时间，两次读取操作的时间间隔如果大于这个值，那么nginx和后端的链接会被关闭，如果一个请求的处理时间比较长，可以把这个值设置得大一些。

   5. proxy_upstream_fail_timeout

      设置了某一个upstream后端失败了指定次数（max_fails）后，在fail_timeout时间内不再去请求它,默认为10秒语法 server address [fail_timeout=30s]。


### 3.5 Nginx动静分离

Nginx支持的静态资源类型见Nginx conf 目录下 mime.types。

用户访问一个网站，然后从服务器端获取相应的资源通过浏览器进行解析渲染最后展示给用户，而服务端可以返回
各种类型的内容，比如xml、jpg、png、gif、flash、MP4、html、css等等，那么浏览器就是根据mime-type来决
定用什么形式来展示的服务器返回的资源给到浏览器时，会把媒体类型告知浏览器，这个告知的标识就是Content-Type，比如Content-Type:text/html。

```
server {
       listen 82;
       server_name localhost;
       location ~ .*\.(gif|jpg|jpeg|bmp|png|ico|js|css){
            root static;
            #配置静态文件缓存时间为1天
            #格式： expires 30s|m|h|d
            expires 1d;
        }
       location / {
           proxy_pass http://localhost:8080;
       }
   }

```

Nginx 配置缓存：

Nginx可以通过expires设置缓存，比如我们可以针对图片做缓存，因为图片这类信息基本上不会改变。
在location中设置expires。

Nginx 配置gzip:

我们一个网站一定会包含很多的静态文件，比如图片、脚本、样式等等，而这些css/js可能本身会比较大，那么在
网络传输的时候就会比较慢，从而导致网站的渲染速度。因此Nginx中提供了一种Gzip的压缩优化手段，可以对后
端的文件进行压缩传输，压缩以后的好处在于能够降低文件的大小来提高传输效率。

```
#是否开启gzip压缩
Gzip on|off 

#设置gzip申请内存的大小
#如下配置表示为大小以16k为单位的4倍申请内存。
Gzip_buffers 4 16k 

#压缩级别， 级别越高，压缩越小，但是会占用CPU资源
Gzip_comp_level[1-9]

#正则匹配UA 表示什么样的浏览器不进行gzip
Gzip_disable 

#开始压缩的最小长度（小于多少就不做压缩），可以指定单位，比如 1k
Gzip_min_length

#表示开始压缩的http协议版本
Gzip_http_version 1.0|1.1 

#nginx 做前端代理时启用该选项，表示无论后端服务器的headers头返回什么信息，都无条件启用压缩
Gzip_proxied

#对那些类型的文件做压缩 （conf/mime.conf）
Gzip_type text/pliain,application/xml 

#是否传输gzip压缩标识; 启用应答头"Vary: Accept-Encoding";给代理服务器用的，有的浏览器支持压缩
#有的不支持，所以避免浪费不支持的也压缩，所以根据客户端的HTTP头来判断，是否需要压缩
Gzip_vary on|off 
```

示例：

```
http {
    ...
    gzip  on;
    gzip_buffers 4 32k;
    gzip_min_length 1k;
    gzip_comp_level 3;
    gzip_types image/png;
    gzip_vary on;
    ...
}
```



### 3.6 防盗链

一个网站上会有很多的图片，如果你不希望其他网站直接用你的图片地址访问自己的图片，或者希望对图片有版权
保护。再或者不希望被第三方调用造成服务器的负载以及消耗比较多的流量问题，那么防盗链就是你必须要做的。

防盗链的语法：

在Nginx中配置防盗链其实很简单，
语法: valid_referers none | blocked | server_names | string ...;
上下文: server, location

“Referer”请求头为指定值时，内嵌变量$invalid_referer被设置为空字符串，否则这个变量会被置成“1”。查找匹配
时不区分大小写，其中none表示缺少referer请求头、blocked表示请求头存在，但是它的值被防火墙或者代理服务器删除、server_names表示referer请求头包含指定的虚拟主机名。

配置如下：

```
server {
       listen 82;
       server_name localhost;
       location ~ .*\.(gif|jpg|jpeg|bmp|png|ico|js|css){
           # proxy_pass http://localhost:8080;
           #若访问的主机不是192.168.25.134禁止对以上配置的静态资源进行访问
           valid_referers none blocked 192.168.25.134;
           if ($invalid_referer){
               return 403;
           }
           root static;
           expires 1d;
        }
       location / {
           proxy_pass http://localhost:8080;
       }
   }
```

需要注意的是伪造一个有效的“Referer”请求头是相当容易的，因此这个模块的预期目的不在于彻底地阻止这些非法请求，而是为了阻止由正常浏览器发出的大规模此类请求。还有一点需要注意，即使正常浏览器发送的合法请求，也可能没有“Referer”请求头。



### 3.7 跨域访问

什么叫跨域呢？如果两个节点的协议、域名、端口、子域名不同，那么进行的操作都是跨域的，浏览器为了安全问
题都是限制跨域访问，所以跨域其实是浏览器本身的限制。

配置：

```
server{
    listen 80;
    server_name localhost;
    location / {
        proxy_pass http://192.168.25.134:8080;
        proxy_set_header Host $host;
        proxy_set_header X-Real-IP $remote_addr;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_send_timeout 60s;
        proxy_read_timeout 60s;
        proxy_connect_timeout 60s;
        #允许来自所有的访问地址
        add_header 'Access-Control-Allow-Origin' '*';
        #支持的请求方式
        add_header 'Access-Control-Allow-Methods' 'GET,PUT,POST,DELETE,OPTIONS'; 
        #支持的媒体类型
        add_header 'Access-Control-Allow-Header' 'Content-Type,*'; 
    }
    location ~ .*\.(gif|jpg|ico|png|css|svg|js)$ {
    root static;
    }
}
```



## 4. Nginx进程模型

nginx在启动后，在unix系统中会以daemon的方式在后台运行，后台进程包含一个master进程和多个worker进程。master进程主要用来管理worker进程，包含：接收来自外界的信号，向各worker进程发送信号，监控worker进程的运行状态，当worker进程退出后(异常情况下)，会自动重新启动新的worker进程。而基本的网络事件，则是放在worker进程中来处理了。多个worker进程之间是对等的，他们同等竞争来自客户端的请求，各进程互相之间是独立的。一个请求，只可能在一个worker进程中处理，一个worker进程，不可能处理其它进程的请求。worker进程的个数是可以设置的，一般我们会设置与机器cpu核数一致，这里面的原因与nginx的进程模型以及事件处理模型是分不开的。nginx的进程模型，可以由下图来表示：

![nginxè¿ç¨æ¨¡å](http://tengine.taobao.org/book/_images/chapter-2-1.PNG)



## 5. Nginx高可用方案 - Keepalived

### 5.1 Keepalived安装

```
1. 下载Keepalived安装包
2. 解压压缩包 tar -zxvf keepalived-2.0.7.tar.gz 
3. 在keepalived解压目录执行 ./configure，若出现错误，根据提示安装缺少的依赖包
4. make && make install
```



## 6. OpenRestry

##  