# Redis学习

## 1. Key

Redis key值是二进制安全的，这意味着可以用任何二进制序列作为key值，从形如”foo”的简单字符串到一个JPEG文件的内容都可以。空字符串也是有效key值。

关于key的几条规则：

- 太长的键值不是个好主意，例如1024字节的键值就不是个好主意，不仅因为消耗内存，而且在数据中查找这类键值的计算成本很高。
- 太短的键值通常也不是好主意，如果你要用”u:1000:pwd”来代替”user:1000:password”，这没有什么问题，但后者更易阅读，并且由此增加的空间消耗相对于key object和value object本身来说很小。当然，没人阻止您一定要用更短的键值节省一丁点儿空间。
- 最好坚持一种模式。例如：”object-type\:id\:field”就是个不错的注意，像这样”user:1000:password”。我喜欢对多单词的字段名中加上一个点，就像这样：”comment\:1234\:reply.to”。
- 允许最大的Key 为512M

> Redis keys are binary safe, this means that you can use any binary sequence as a key, from a string like "foo" to the content of a JPEG file. The empty string is also a valid key.
>
> A few other rules about keys:
>
> - Very long keys are not a good idea. For instance a key of 1024 bytes is a bad idea not only memory-wise, but also because the lookup of the key in the dataset may require several costly key-comparisons. Even when the task at hand is to match the existence of a large value, hashing it (for example with SHA1) is a better idea, especially from the perspective of memory and bandwidth.
> - Very short keys are often not a good idea. There is little point in writing "u1000flw" as a key if you can instead write "user:1000:followers". The latter is more readable and the added space is minor compared to the space used by the key object itself and the value object. While short keys will obviously consume a bit less memory, your job is to find the right balance.
> - Try to stick with a schema. For instance "object-type:id" is a good idea, as in "user:1000". Dots or dashes are often used for multi-word fields, as in "comment\:1234\:reply.to" or "comment\:1234\:reply-to".
> - The maximum allowed key size is 512 MB.



## 2. 数据类型

### 2.1 String

[Redis-String](https://redis.io/topics/data-types-intro#strings)

> The Redis String type is the simplest type of value you can associate with a Redis key. It is the only data type in Memcached, so it is also very natural for newcomers to use it in Redis.
>
> Since Redis keys are strings, when we use the string type as a value too, we are mapping a string to another string. The string data type is useful for a number of use cases, like caching HTML fragments or pages.
>
> As you can see using the [SET](https://redis.io/commands/set) and the [GET](https://redis.io/commands/get) commands are the way we set and retrieve a string value. Note that [SET](https://redis.io/commands/set) will replace any existing value already stored into the key, in the case that the key already exists, even if the key is associated with a non-string value. So [SET](https://redis.io/commands/set) performs an assignment.
>
> Values can be strings (including binary data) of every kind, for instance you can store a jpeg image inside a value. A value can't be bigger than 512 MB.
>
> The [SET](https://redis.io/commands/set) command has interesting options, that are provided as additional arguments. For example, I may ask [SET](https://redis.io/commands/set) to fail if the key already exists, or the opposite, that it only succeed if the key already exists:
>
> ```shell
> > set mykey newval nx
> (nil)
> > set mykey newval xx
> OK
> ```
>
> Even if strings are the basic values of Redis, there are interesting operations you can perform with them. For instance, one is atomic increment:
>
> ```shell
> > set counter 100
> OK
> > incr counter
> (integer) 101
> > incr counter
> (integer) 102
> > incrby counter 50
> (integer) 152
> ```
>
> ....



### 2.2 List

[Redis-List](https://redis.io/topics/data-types-intro#lists)

> Redis Lists are implemented with linked lists because for a database system it is crucial to be able to add elements to a very long list in a very fast way. Another strong advantage, as you'll see in a moment, is that Redis Lists can be taken at constant length in constant time.



### 2.3 Hash

[Redis-Hash](https://redis.io/topics/data-types-intro#hashes)



### 2.4 Set

[Redis-Set](https://redis.io/topics/data-types-intro#sets)



### 2.5 Sorted-Set

[Redis-Sorted-Set](https://redis.io/topics/data-types-intro#sorted-sets)



### 2.6 Bit-Map

[Redis-Bit-Map](https://redis.io/topics/data-types-intro#bitmaps)



### 2.7 HyperLogLogs

[HyperLogLogs](https://redis.io/topics/data-types-intro#hyperloglogs)



### 2.8 GEO

[GEO](https://redis.io/commands/geoadd)(**Available since 3.2.0**)



## 3. 数据持久化

### 3.1 快照持久化

Redis可以通过创建快照开获得存储在内存里面的数据在某个时间节点上的副本。

如果在新的快照文件创建完毕之前，Redis、系统或者硬件这三者之中任意一个崩溃了，那么Redis将丢失最近一次创建快照之后写入的所有数据。

#### 3.1.1 创建快照的方法

- BGSAVE: Redis会fork一个子进程，子进程负责将快照写入硬盘，父进程继续处理命令请求。

- SAVE: Redis在快照创建完毕之前不再响应任何其他命令（速度会比BGSAVE快）。

- redis.config 配置save选项，任意一个save选项满足时，触发BGSAVE。

  ```properties
  save 900 1
  save 300 10
  save 60 10000
  ```

- Redis通过SHUTDOWN命令接收到关闭服务器请求时或者接收到标准TERM信号时，会执行SAVE命令，阻塞所有客户端，不再执行客户端发送的任何命令，并在SAVE命令执行完毕后关闭服务器。

- 当一个Redis服务器连接另外一个Redis服务器，并向对方发送SYNC命令来开始一次复制操作时，如果主服务器目前没有在执行BGSAVE操作，或者主服务器并非刚刚执行完BGSAVE操作，那么主服务器就会执行BGSAVE命令。



### 3.2 AOF持久化

#### 3.2.1 AOF使用

AOF持久化会将被执行的写命令写到AOF文件的末尾，以此来记录数据发生的变化。

```properties
appendfsync always #每个Redis写命令都要同步写入磁盘（写入数据量较大时不建议使用）
appendfsync everysec #每秒执行一次同步，显式的将多个写命令同步到硬盘
appendfsync no #让操作系统来决定何时进行同步（不推荐）
```

#### 3.2.2 重写/压缩 AOF 文件

- BGREWRITEAOF: Redis会创建一个子进程，由子进程负责对AOF文件进行重写

- redis.config

  ```properties
  auto-aof-rewrite-percentage 100 #当AOF文件体积比上一次重写之后的体积大了至少(100%)时，Redis将执行BGREWRITEAOF命令
  auto-aof-rewrite-min-size 64mb #当AOF文件大于64M时，将执行BGREWRITEAOF命令。
  ```



## 4. Redis 事务

redis事务是通过MULTI，EXEC，DISCARD和WATCH四个原语实现的。

MULTI命令用于开启一个事务，它总是返回OK。

MULTI执行之后，客户端可以继续向服务器发送任意多条命令，这些命令不会立即被执行，而是被放到一个队列中，当EXEC命令被调用时，所有队列中的命令才会被执行。

另一方面，通过调用DISCARD，客户端可以清空事务队列，并放弃执行事务。

- 正常执行

  ```shell
  127.0.0.1:6379> MULTI
  OK
  127.0.0.1:6379> SET key1 1
  QUEUED
  127.0.0.1:6379> HSET key2 field1 1
  QUEUED
  127.0.0.1:6379> SADD key3 1
  QUEUED
  127.0.0.1:6379> EXEC
  1) OK
  2) (integer) 1
  3) (integer) 1
  ```

  EXEC 命令的回复是一个数组，数组中的每个元素都是执行事务中的命令所产生的回复。 其中，回复元素的先后顺序和命令发送的先后顺序一致。

  当客户端处于事务状态时，所有传入的命令都会返回一个内容为 QUEUED 的状态回复（status reply），这些被入队的命令将在 EXEC命令被调用时执行。

- 放弃事务

  ```shell
  127.0.0.1:6379> MULTI
  OK
  127.0.0.1:6379> SET key1 1
  QUEUED
  127.0.0.1:6379> DISCARD
  OK
  127.0.0.1:6379> EXEC
  (error) ERR EXEC without MULTI
  ```

  当执行 DISCARD 命令时，事务会被放弃，事务队列会被清空，并且客户端会从事务状态中退出。

- 入队错误回滚

  ```shell
  127.0.0.1:6379> MULTI
  OK
  127.0.0.1:6379> set key1 1
  QUEUED
  127.0.0.1:6379> HSET key2 1
  (error) ERR wrong number of arguments for 'hset' command
  127.0.0.1:6379> SADD key3 1
  QUEUED
  127.0.0.1:6379> EXEC
  (error) EXECABORT Transaction discarded because of previous errors.
  ```

  对于入队错误，redis 2.6.5版本后，会记录这种错误，并且在执行EXEC的时候，报错并回滚事务中所有的命令，并且终止事务。

- 执行错误放过

  ```shell
  127.0.0.1:6379> MULTI
  OK
  127.0.0.1:6379> HSET key1 field1 1
  QUEUED
  127.0.0.1:6379> HSET key2 field1 1
  QUEUED
  127.0.0.1:6379> EXEC
  1) (error) WRONGTYPE Operation against a key holding the wrong kind of value
  2) (integer) 1
  ```

  当遇到执行错误时，redis放过这种错误，保证事务执行完成。

  这里要注意此问题，与mysql中事务不同，在redis事务遇到执行错误的时候，不会进行回滚，而是简单的放过了，并保证其他的命令正常执行。这个区别在实现业务的时候，需要自己保证逻辑符合预期。

- WATCH

  WATCH 命令可以为 Redis 事务提供 check-and-set （CAS）行为。相当于一个乐观锁

  被 [WATCH](http://doc.redisfans.com/transaction/watch.html#watch) 的键会被监视，并会发觉这些键是否被改动过了。 如果有至少一个被监视的键在 [EXEC](http://doc.redisfans.com/transaction/exec.html#exec) 执行之前被修改了， 那么整个事务都会被取消， [EXEC](http://doc.redisfans.com/transaction/exec.html#exec) 返回空多条批量回复（null multi-bulk reply）来表示事务已经失败。

  ```shell
  127.0.0.1:6379> WATCH key1
  OK
  127.0.0.1:6379> set key1 2
  OK
  127.0.0.1:6379> MULTI
  OK
  127.0.0.1:6379> set key1 3
  QUEUED
  127.0.0.1:6379> set key2 3
  QUEUED
  127.0.0.1:6379> EXEC
  (nil)
  ```




## 5. Redis 集群

### 5.1 主从复制

复制的作用是把redis的数据库复制多个副本部署在不同的服务器上，如果其中一台服务器出现故障，也能快速迁
移到其他服务器上提供服务。 复制功能可以实现当一台redis服务器的数据更新后，自动将新的数据同步到其他服
务器上。
主从复制就是我们常见的master/slave模式， 主数据库可以进行读写操作，当写操作导致数据发生变化时会自动将数据同步给从数据库。而一般情况下，从数据库是只读的，并接收主数据库同步过来的数据。 一个主数据库可以有多个从数据库。

#### 5.1.1 配置

在redis中配置master/slave是非常容易的，只需要在从数据库的配置文件中加入slaveof 主数据库地址 端口。 而
master 数据库不需要做任何改变。

```
准备两台服务器server1 server2，分别安装redis 
1. 在server2的redis.conf文件中增加 slaveof server1-ip 6379 、 同时将bindip注释掉，允许所
有ip访问
2. 启动server2
3. 访问server2的redis客户端，输入 INFO replication
4. 通过在master机器上输入命令，比如set foo bar 、 在slave服务器就能看到该值已经同步过来了
```

#### 5.1.2 实现

##### 5.1.2.1 全量复制

Redis全量复制一般发生在Slave初始化阶段，这时Slave需要将Master上的所有数据都复制一份，复制步骤如下：

![1539041304034](E:\gupao2\notes\图片\redis-主从复制.png)

完成上面几个步骤后就完成了slave服务器数据初始化的所有操作，savle服务器此时可以接收来自用户的读请求。

master/slave 复制策略是采用乐观复制，也就是说可以容忍在一定时间内master/slave数据的内容是不同的，但是两者的数据会最终同步。具体来说，redis的主从同步过程本身是异步的，意味着master执行完客户端请求的命令后会立即返回结果给客户端，然后异步的方式把命令同步给slave。
这一特征保证启用master/slave后 master的性能不会受到影响。
但是另一方面，如果在这个数据不一致的窗口期间，master/slave因为网络问题断开连接，而这个时候，master
是无法得知某个命令最终同步给了多少个slave数据库。不过redis提供了一个配置项来限制只有数据至少同步给多
少个slave的时候，master才是可写的：
`min-slaves-to-write 3` 表示只有当3个或以上的slave连接到master，master才是可写的
`min-slaves-max-lag 10` 表示允许slave最长失去连接的时间，如果10秒还没收到slave的响应，则master认为该slave以断开。



##### 5.1.2.2 增量复制

从redis 2.8开始，就支持主从复制的断点续传，如果主从复制过程中，网络连接断掉了，那么可以接着上次复制的
地方，继续复制下去，而不是从头开始复制一份。
master node会在内存中创建一个backlog，master和slave都会保存一个replica offset还有一个master id，offset就是保存在backlog中的。如果master和slave网络连接断掉了，slave会让master从上次的replica offset开始继续复制。
但是如果没有找到对应的offset，那么就会执行一次全量同步。



##### 5.1.2.3 无硬盘复制

前面我们说过，Redis复制的工作原理基于RDB方式的持久化实现的，也就是master在后台保存RDB快照，slave接收到rdb文件并载入，但是这种方式会存在一些问题。

1. 当master禁用RDB时，如果执行了复制初始化操作，Redis依然会生成RDB快照，当master下次启动时执行该
   RDB文件的恢复，但是因为复制发生的时间点不确定，所以恢复的数据可能是任何时间点的。就会造成数据出现问题
2. 当硬盘性能比较慢的情况下（网络硬盘），那初始化复制过程会对性能产生影响

因此2.8.18以后的版本，Redis引入了无硬盘复制选项，可以不需要通过RDB文件去同步，直接发送数据，通过以
下配置来开启该功能。

```properties
repl-diskless-sync yes
```

master在内存中直接创建rdb，然后发送给slave，不会在自己本地落地磁盘了。

### 5.2 哨兵机制

在一个典型的一主多从的系统中，slave在整个体系中起到了数据冗余备份和读写分离的作用。当master遇到异常终端后，需要从slave中选举一个新的master继续对外提供服务。redis并没有提供自动master选举功能，而是需要借助一个哨兵来进行监控。

#### 5.2.1 什么是哨兵

顾名思义，哨兵的作用就是监控Redis系统的运行状况，它的功能包括两个

1. 监控master和slave是否正常运行
2. master出现故障时自动将slave数据库升级为master

哨兵是一个独立的进程，使用哨兵后的架构图：

![](E:\gupao2\notes\图片\redis-哨兵.png)

为了解决master选举问题，又引出了一个单点问题，也就是哨兵的可用性如何解决，在一个一主多从的Redis系统
中，可以使用多个哨兵进行监控任务以保证系统足够稳定。此时哨兵不仅会监控master和slave，同时还会互相监
控；这种方式称为哨兵集群，哨兵集群需要解决故障发现、和master决策的协商机制问题

![](E:\gupao2\notes\图片\redis-哨兵集群.png)

sentinel之间的相互感知

sentinel节点之间会因为共同监视同一个master从而产生了关联，一个新加入的sentinel节点需要和其他监视相同
master节点的sentinel相互感知

1. 需要相互感知的sentinel都向他们共同监视的master节点订阅channel:sentinel:hello
2. 新加入的sentinel节点向这个channel发布一条消息，包含自己本身的信息，这样订阅了这个channel的sentinel就可以发现这个新的sentinel
3. 新加入得sentinel和其他sentinel节点建立长连接

![哨兵感知](E:\gupao2\notes\图片\redis-哨兵感知.png)

#### 5.2.2 master的故障发现

sentinel节点会定期向master节点发送心跳包来判断存活状态，一旦master节点没有正确响应，sentinel会把
master设置为“主观不可用状态”，然后它会把“主观不可用”发送给其他所有的sentinel节点去确认，当确认的
sentinel节点数大于>quorum时，则会认为master是“客观不可用”，接着就开始进入选举新的master流程；但是
这里又会遇到一个问题，就是sentinel中，本身是一个集群，如果多个节点同时发现master节点达到客观不可用状
态，那谁来决策选择哪个节点作为maste呢？这个时候就需要从sentinel集群中选择一个leader来做决策。而这里
用到了一致性算法Raft算法、它和Paxos算法类似，都是分布式一致性算法。但是它比Paxos算法要更容易理解；
Raft和Paxos算法一样，也是基于投票算法，只要保证过半数节点通过提议即可;
[动画演示地址](http://thesecretlivesofdata.com/raft/)

#### 5.2.3 配置实现

通过在这个配置的基础上增加哨兵机制。在其中任意一台服务器上创建一个sentinel.conf文件，文件内容：

```shell
sentinel monitor name ip port quorum
#sentinel monitor mymaster 192.168.11.131 6379 1
sentinel down-after-milliseconds mymaster 5000 --表示如果5s内mymaster没响应，就认为SDOWN
sentinel failover-timeout mymaster 15000 --表示如果15秒后,mysater仍没活过来，则启动failover，从剩下的slave中选一个升级为master
```

其中name表示要监控的master的名字，这个名字是自己定义。 ip和port表示master的ip和端口号。 最后一个1表示最低通过票数，也就是说至少需要几个哨兵节点统一才可以。

两种方式启动哨兵：

- `redis-sentinel sentinel.conf`

- `redis-server /path/to/sentinel.conf --sentinel`

哨兵监控一个系统时，只需要配置监控master即可，哨兵会自动发现所有slave。



### 5.3 Redis-Cluster

即使是使用哨兵，此时的Redis集群的每个数据库依然存有集群中的所有数据，从而导致集群的总数据存储量受限
于可用存储内存最小的节点，形成了木桶效应。而因为Redis是基于内存存储的，所以这一个问题在redis中就显得
尤为突出了。

在redis3.0之前，我们是通过在客户端去做的分片，通过hash环的方式对key进行分片存储。分片虽然能够解决各
个节点的存储压力，但是导致维护成本高、增加、移除节点比较繁琐。因此在redis3.0以后的版本最大的一个好处
就是支持集群功能，集群的特点在于拥有和单机实例一样的性能，同时在网络分区以后能够提供一定的可访问性以
及对主数据库故障恢复的支持。

哨兵和集群是两个独立的功能，当不需要对数据进行分片使用哨兵就够了，如果要进行水平扩容，集群是一个比较
好的方式。

#### 5.3.1 拓扑结构

一个Redis Cluster由多个Redis节点构成。不同节点组服务的数据没有交集，也就是每个一节点组对应数据
sharding的一个分片。节点组内部分为主备两类节点，对应master和slave节点。两者数据准实时一致，通过异步
化的主备复制机制来保证。一个节点组有且只有一个master节点，同时可以有0到多个slave节点，在这个节点组中只有master节点对用户提供些服务，读服务可以由master或者slave提供。

redis-cluster是基于gossip协议实现的无中心化节点的集群，因为去中心化的架构不存在统一的配置中心，各个节
点对整个集群状态的认知来自于节点之间的信息交互。在Redis Cluster，这个信息交互是通过Redis Cluster Bus来完成的。

#### 5.3.2 Redis的数据分区

分布式数据库首要解决把整个数据集按照分区规则映射到多个节点的问题，即把数据集划分到多个节点上，每个节
点负责整个数据的一个子集, Redis Cluster采用哈希分区规则,采用虚拟槽分区。

虚拟槽分区巧妙地使用了哈希空间，使用分散度良好的哈希函数把所有的数据映射到一个固定范围内的整数集合，
整数定义为槽（slot）。比如Redis Cluster槽的范围是0 ～ 16383（2^14）。槽是集群内数据管理和迁移的基本单位。采用大范围的槽的主要目的是为了方便数据的拆分和集群的扩展，每个节点负责一定数量的槽。

计算公式：slot = CRC16(key)%16383。每一个节点负责维护一部分槽以及槽所映射的键值数据。

#### 5.3.3 HashTags

通过分片手段，可以将数据合理的划分到不同的节点上，这本来是一件好事。但是有的时候，我们希望对相关联的
业务以原子方式进行操作。举个简单的例子:

我们在单节点上执行MSET , 它是一个原子性的操作，所有给定的key会在同一时间内被设置，不可能出现某些指定
的key被更新另一些指定的key没有改变的情况。但是在集群环境下，我们仍然可以执行MSET命令，但它的操作不
在是原子操作，会存在某些指定的key被更新，而另外一些指定的key没有改变，原因是多个key可能会被分配到不
同的机器上。

所以，这里就会存在一个矛盾点，及要求key尽可能的分散在不同机器，又要求某些相关联的key分配到相同机器。这个也是在面试的时候会容易被问到的内容。怎么解决呢？

从前面的分析中我们了解到，分片其实就是一个hash的过程，对key做hash取模然后划分到不同的机器上。所以为了解决这个问题，我们需要考虑如何让相关联的key得到的hash值都相同呢？如果key全部相同是不现实的，所以怎么解决呢？在redis中引入了HashTag的概念，可以使得数据分布算法可以根据key的某一个部分进行计算，然后让相关的key落到同一个数据分片。

举个简单的例子，加入对于用户的信息进行存储， user:user1:id、user:user1:name/ 那么通过hashtag的方式，
user:{user1}:id、user:{user1}.name; 表示
当一个key包含 {} 的时候，就不对整个key做hash，而仅对 {} 包括的字符串做hash。

5.3.4 重定向



#### 5.3.4 重定向客户端

Redis Cluster并不会代理查询，那么如果客户端访问了一个key并不存在的节点，这个节点是怎么处理的呢？比如
我想获取key为msg的值，msg计算出来的槽编号为254，当前节点正好不负责编号为254的槽，那么就会返回客户
端下面信息：
-MOVED 254 127.0.0.1:6381
表示客户端想要的254槽由运行在IP为127.0.0.1，端口为6381的Master实例服务。如果根据key计算得出的槽恰好
由当前节点负责，则当期节点会立即返回结果。

#### 5.3.5 分片迁移

在一个稳定的Redis cluster下，每一个slot对应的节点是确定的，但是在某些情况下，节点和分片对应的关系会发
生变更：

- 新加入master节点
- 某个节点宕机

也就是说当动态添加或减少node节点时，需要将16384个槽做个再分配，槽中的键值也要迁移。当然，这一过程，在目前实现中，还处于半自动状态，需要人工介入。

新增一个节点：

假设新增一个节点D，redis cluster的这种做法是从各个节点的前面各拿取一部分slot到D上。大致就会变成这样：
节点A覆盖1365-5460
节点B覆盖6827-10922
节点C覆盖12288-16383
节点D覆盖0-1364,5461-6826,10923-12287

删除一个节点：

先将节点的数据移动到其他节点上，然后才能执行删除

#### 5.3.6 槽迁移过程

槽迁移的过程中有一个不稳定状态，这个不稳定状态会有一些规则，这些规则定义客户端的行为，从而使得Redis
Cluster不必宕机的情况下可以执行槽的迁移。下面这张图描述了我们迁移编号为1、2、3的槽的过程中，他们在
MasterA节点和MasterB节点中的状态。

![槽迁移](E:\gupao2\notes\图片\redis-槽迁移.png)

简单的工作流程

1. 向MasterB发送状态变更命令，把Master B对应的slot状态设置为IMPORTING

2. 向MasterA发送状态变更命令，将Master对应的slot状态设置为MIGRATING

   当MasterA的状态设置为MIGRANTING后，表示对应的slot正在迁移，为了保证slot数据的一致性，MasterA此时对于slot内部数据提供读写服务的行为和通常状态下是有区别的，

##### 5.3.6.1 MIGRATING状态

1. 如果客户端访问的Key还没有迁移出去，则正常处理这个key
2. 如果key已经迁移或者根本就不存在这个key，则回复客户端ASK信息让它跳转到MasterB去执行

##### 5.3.6.2 IMPORTING状态

当MasterB的状态设置为IMPORTING后，表示对应的slot正在向MasterB迁入，即使Master仍然能对外提供该slot
的读写服务，但和通常状态下也是有区别的。

当来自客户端的正常访问不是从ASK跳转过来的，说明客户端还不知道迁移正在进行，很有可能操作了一个目前还没迁移完成的并且还存在于MasterA上的key，如果此时这个key在A上已经被修改了，那么B和A的修改则会发生冲突。所以对于MasterB上的slot上的所有非ASK跳转过来的操作，MasterB都不会出去处理，而是通过MOVED命令让客户端跳转到MasterA上去执行。

这样的状态控制保证了同一个key在迁移之前总是在源节点上执行，迁移后总是在目标节点上执行，防止出现两边
同时写导致的冲突问题。而且迁移过程中新增的key一定会在目标节点上执行，源节点也不会新增key，是的整个迁移过程既能对外正常提供服务，又能在一定的时间点完成slot的迁移。

## 6. Redis java 客户端

Redis Java客户端有很多的开源产品比如Redission、Jedis、lettuce。

### 6.1 java客户端差异

Jedis是Redis的Java实现的客户端，其API提供了比较全面的Redis命令的支持。

Redisson实现了分布式和可扩展的Java数据结构，和Jedis相比，功能较为简单，不支持字符串操作，不支持排
序、事务、管道、分区等Redis特性。Redisson主要是促进使用者对Redis的关注分离，从而让使用者能够将精力更集中地放在处理业务逻辑上。

lettuce是基于Netty构建的一个可伸缩的线程安全的Redis客户端，支持同步、异步、响应式模式。多个线程可以
共享一个连接实例，而不必担心多线程并发问题。

### 6.2 Jedis

#### 6.2.1 Jedis-Sentinel

##### 6.2.1.1 原理

客户端通过连接到哨兵集群，通过发送Protocol.SENTINEL_GET_MASTER_ADDR_BY_NAME 命令，从哨兵机器中
询问master节点的信息，拿到master节点的ip和端口号以后，再到客户端发起连接。连接以后，需要在客户端建
立监听机制，当master重新选举之后，客户端需要重新连接到新的master节点。

##### 6.2.1.2 源码分析

初始化哨兵节点：

```java
private HostAndPort initSentinels(Set<String> sentinels, final String masterName) {

    HostAndPort master = null;
    boolean sentinelAvailable = false;

    log.info("Trying to find master from available Sentinels...");
	//遍历sentinels
    for (String sentinel : sentinels) {
      //解析sentinel获得HostAndPort,即ip和端口号
      final HostAndPort hap = HostAndPort.parseString(sentinel);

      log.fine("Connecting to Sentinel " + hap);

      Jedis jedis = null;
      try {
        //连接到sentinel
        jedis = new Jedis(hap.getHost(), hap.getPort());
		//根据masterName获取master地址，返回host=masterAddr.get(0), port=masterAddr.get(1)
        List<String> masterAddr = jedis.sentinelGetMasterAddrByName(masterName);

        // connected to sentinel...
        sentinelAvailable = true;

        if (masterAddr == null || masterAddr.size() != 2) {
          log.warning("Can not get master addr, master name: " + masterName + ". Sentinel: " + hap
              + ".");
          continue;
        }
		//如何在任意一个sentinel中获取到master，不在遍历sentinel
        master = toHostAndPort(masterAddr);
        log.fine("Found Redis master at " + master);
        break;
      } catch (JedisException e) {
        // resolves #1036, it should handle JedisException there's another chance
        // of raising JedisDataException
        log.warning("Cannot get master address from sentinel running @ " + hap + ". Reason: " + e
            + ". Trying next one.");
      } finally {
        if (jedis != null) {
          jedis.close();
        }
      }
    }
    //如果master为空，说明2中情况
    //1. 所有的sentinel节点全部down掉
    //2. master节点未被存活的sentinel节点监控到
    if (master == null) {
      if (sentinelAvailable) {
        // can connect to sentinel, but master name seems to not
        // monitored
        throw new JedisException("Can connect to sentinel, but " + masterName
            + " seems to be not monitored...");
      } else {
        throw new JedisConnectionException("All sentinels down, cannot determine where is "
            + masterName + " master is running...");
      }
    }

    log.info("Redis master running at " + master + ", starting Sentinel listeners...");
	//为每个sentinel都启动了一个监听者MasterListener。MasterListener本身是一个线程，它会去订阅sentinel上关于master节点地址改变的消息。
    for (String sentinel : sentinels) {
      final HostAndPort hap = HostAndPort.parseString(sentinel);
      MasterListener masterListener = new MasterListener(masterName, hap.getHost(), hap.getPort());
      // whether MasterListener threads are alive or not, process can be stopped
      masterListener.setDaemon(true);
      masterListeners.add(masterListener);
      masterListener.start();
    }

    return master;
  }
```

从哨兵节点中获取master的方法：

```java
public List<String> sentinelGetMasterAddrByName(String masterName) {
    client.sentinel(Protocol.SENTINEL_GET_MASTER_ADDR_BY_NAME, masterName);
    final List<Object> reply = client.getObjectMultiBulkReply();
    return BuilderFactory.STRING_LIST.build(reply);
  }
```

#### 6.2.2 Jedis-Cluster

##### 6.2.2.1 原理分析

初始化集群环境：

- 读取配置文件中的节点配置，无论是主从，无论多少个，只拿第一个，获取redis连接实例
- 用获取的redis连接实例执行clusterNodes()方法，实际执行redis服务端cluster nodes命令，获取主从配置信
  息
- 解析主从配置信息，先把所有节点存放到nodes的map集合中，key为节点的ip:port，value为当前节点的
  jedisPool
- 解析主节点分配的slots区间段，把slot对应的索引值作为key，第三步中拿到的jedisPool作为value，存储在
  slots的map集合中，实现了slot槽索引值与jedisPool的映射，这个jedisPool包含了master的节点信息，所以槽和几点是对应的，与redis服务端一致。

从集群环境存取值：

- 把key作为参数，执行CRC16算法，获取key对应的slot值
- 通过该slot值，去slots的map集合中获取jedisPool实例
- 通过jedisPool实例获取jedis实例，最终完成redis数据存取工作

#### 6.2.3 管道模式

Redis服务是一种C/S模型，提供请求－响应式协议的TCP服务，所以当客户端发起请求，服务端处理并返回结果到
客户端，一般是以阻塞形式等待服务端的响应，但这在批量处理连接时延迟问题比较严重，所以Redis为了提升或
弥补这个问题，引入了管道技术：可以做到服务端未及时响应的时候，客户端也可以继续发送命令请求，做到客户
端和服务端互不影响，服务端并最终返回所有服务端的响应，大大提高了C/S模型交互的响应速度上有了质的提高。

```java
Jedis jedis=new Jedis("192.168.3.223",6379);
Pipeline pipeline=jedis.pipelined();
for(int i=0;i<1000;i++){
pipeline.incr("test");
}
pipeline.sync();
```

### 6.3 Redisson

#### 6.3.1 Redisson常用命令

```
getBucket-> 获取字符串对象；
getMap -> 获取map对象
getSortedSet->获取有序集合
getSet -> 获取集合
getList ->获取列表
```

#### 6.3.2 Redisson实现分布式锁

Redisson它除了常规的操作命令以外，还基于redis本身的特性去实现了很多功能的封装，比如分布式锁、原子操
作、布隆过滤器、队列等等。我们可以直接利用这个api提供的功能去实现。

```java
public static void main(String[] args) throws InterruptedException {
    Config config = new Config();
    config.useSingleServer().setAddress("redis://192.168.3.223:6379");
    RedissonClient rc = Redisson.create(config);

    RLock lock = rc.getLock("lock");
    lock.tryLock(100,10, TimeUnit.SECONDS);
    System.out.println("locked");
    lock.unlock();
    System.out.println("release lock");

    rc.shutdown();
}
```

原理分析

tryLock:

```java
public boolean tryLock(long waitTime, long leaseTime, TimeUnit unit) throws InterruptedException {
        long time = unit.toMillis(waitTime);
        long current = System.currentTimeMillis();
        final long threadId = Thread.currentThread().getId();
    	//尝试获取锁
        Long ttl = tryAcquire(leaseTime, unit, threadId);
        // lock acquired
        if (ttl == null) {
            return true;
        }
        
        time -= (System.currentTimeMillis() - current);
        if (time <= 0) {
            acquireFailed(threadId);
            return false;
        }
        
        current = System.currentTimeMillis();
        final RFuture<RedissonLockEntry> subscribeFuture = subscribe(threadId);
        if (!await(subscribeFuture, time, TimeUnit.MILLISECONDS)) {
            if (!subscribeFuture.cancel(false)) {
                subscribeFuture.addListener(new FutureListener<RedissonLockEntry>() {
                    @Override
                    public void operationComplete(Future<RedissonLockEntry> future) throws Exception {
                        if (subscribeFuture.isSuccess()) {
                            unsubscribe(subscribeFuture, threadId);
                        }
                    }
                });
            }
            acquireFailed(threadId);
            return false;
        }

        try {
            time -= (System.currentTimeMillis() - current);
            if (time <= 0) {
                acquireFailed(threadId);
                return false;
            }
        	//类似自旋来获取锁
            while (true) {
                long currentTime = System.currentTimeMillis();
                ttl = tryAcquire(leaseTime, unit, threadId);
                // lock acquired
                if (ttl == null) {
                    return true;
                }

                time -= (System.currentTimeMillis() - currentTime);
                if (time <= 0) {
                    acquireFailed(threadId);
                    return false;
                }

                // waiting for message
                currentTime = System.currentTimeMillis();
                if (ttl >= 0 && ttl < time) {
                    getEntry(threadId).getLatch().tryAcquire(ttl, TimeUnit.MILLISECONDS);
                } else {
                    getEntry(threadId).getLatch().tryAcquire(time, TimeUnit.MILLISECONDS);
                }

                time -= (System.currentTimeMillis() - currentTime);
                if (time <= 0) {
                    acquireFailed(threadId);
                    return false;
                }
            }
        } finally {
            unsubscribe(subscribeFuture, threadId);
        }
//        return get(tryLockAsync(waitTime, leaseTime, unit));
    }
```

tryAcquireAsync：

```java
private <T> RFuture<Long> tryAcquireAsync(long leaseTime, TimeUnit unit, final long threadId) {
        if (leaseTime != -1) {
            return tryLockInnerAsync(leaseTime, unit, threadId, RedisCommands.EVAL_LONG);
        }
        RFuture<Long> ttlRemainingFuture = tryLockInnerAsync(commandExecutor.getConnectionManager().getCfg().getLockWatchdogTimeout(), TimeUnit.MILLISECONDS, threadId, RedisCommands.EVAL_LONG);
        ttlRemainingFuture.addListener(new FutureListener<Long>() {
            @Override
            public void operationComplete(Future<Long> future) throws Exception {
                if (!future.isSuccess()) {
                    return;
                }

                Long ttlRemaining = future.getNow();
                // lock acquired
                if (ttlRemaining == null) {
                    scheduleExpirationRenewal(threadId);
                }
            }
        });
        return ttlRemainingFuture;
    }
```

tryLockInnerAsync：

采用lua脚本实现获取锁的操作：

1. 判断lock键是否存在，不存在直接调用hset存储当前线程信息并且设置过期时间,返回nil，告诉客户端直接获取到锁。
2. 判断lock键是否存在，存在则将重入次数加1，并重新设置过期时间，返回nil，告诉客户端直接获取到锁。
3. 被其它线程已经锁定，返回锁有效期的剩余时间，告诉客户端需要等待。

```java
<T> RFuture<T> tryLockInnerAsync(long leaseTime, TimeUnit unit, long threadId, RedisStrictCommand<T> command) {
        internalLockLeaseTime = unit.toMillis(leaseTime);

        return commandExecutor.evalWriteAsync(getName(), LongCodec.INSTANCE, command,
                  "if (redis.call('exists', KEYS[1]) == 0) then " +
                      "redis.call('hset', KEYS[1], ARGV[2], 1); " +
                      "redis.call('pexpire', KEYS[1], ARGV[1]); " +
                      "return nil; " +
                  "end; " +
                  "if (redis.call('hexists', KEYS[1], ARGV[2]) == 1) then " +
                      "redis.call('hincrby', KEYS[1], ARGV[2], 1); " +
                      "redis.call('pexpire', KEYS[1], ARGV[1]); " +
                      "return nil; " +
                  "end; " +
                  "return redis.call('pttl', KEYS[1]);",
                    Collections.<Object>singletonList(getName()), internalLockLeaseTime, getLockName(threadId));
    }
```



unlock：

```java
 public void unlock() {
        try {
            get(unlockAsync(Thread.currentThread().getId()));
        } catch (RedisException e) {
            if (e.getCause() instanceof IllegalMonitorStateException) {
                throw (IllegalMonitorStateException)e.getCause();
            } else {
                throw e;
            }
        }
    }
```

unlockAsync：

```java
public RFuture<Void> unlockAsync(final long threadId) {
        final RPromise<Void> result = new RedissonPromise<Void>();
        RFuture<Boolean> future = unlockInnerAsync(threadId);

        future.addListener(new FutureListener<Boolean>() {
            @Override
            public void operationComplete(Future<Boolean> future) throws Exception {
                if (!future.isSuccess()) {
                    cancelExpirationRenewal(threadId);
                    result.tryFailure(future.cause());
                    return;
                }

                Boolean opStatus = future.getNow();
                if (opStatus == null) {
                    IllegalMonitorStateException cause = new IllegalMonitorStateException("attempt to unlock lock, not locked by current thread by node id: "
                            + id + " thread-id: " + threadId);
                    result.tryFailure(cause);
                    return;
                }
                if (opStatus) {
                    cancelExpirationRenewal(null);
                }
                result.trySuccess(null);
            }
        });

        return result;
    }
```

unlockInnerAsync：

使用lua脚本实现：

1. 如果lock键不存在，发消息说锁已经可用，发送一个消息
2. 如果锁不是被当前线程锁定，则返回nil
3. 由于支持可重入，在解锁时将重入次数需要减1
4. 如果计算后的重入次数>0，则重新设置过期时间
5. 如果计算后的重入次数<=0，则发消息说锁已经可用

```java
protected RFuture<Boolean> unlockInnerAsync(long threadId) {
        return commandExecutor.evalWriteAsync(getName(), LongCodec.INSTANCE, RedisCommands.EVAL_BOOLEAN,
                "if (redis.call('exists', KEYS[1]) == 0) then " +
                    "redis.call('publish', KEYS[2], ARGV[1]); " +
                    "return 1; " +
                "end;" +
                "if (redis.call('hexists', KEYS[1], ARGV[3]) == 0) then " +
                    "return nil;" +
                "end; " +
                "local counter = redis.call('hincrby', KEYS[1], ARGV[3], -1); " +
                "if (counter > 0) then " +
                    "redis.call('pexpire', KEYS[1], ARGV[2]); " +
                    "return 0; " +
                "else " +
                    "redis.call('del', KEYS[1]); " +
                    "redis.call('publish', KEYS[2], ARGV[1]); " +
                    "return 1; "+
                "end; " +
                "return nil;",
                Arrays.<Object>asList(getName(), getChannelName()), LockPubSub.unlockMessage, internalLockLeaseTime, getLockName(threadId));

    }
```



## 7. Redis的应用架构

对于读多写少的高并发场景，我们会经常使用缓存来进行优化。比如说支付宝的余额展示功能，实际上99%的时候
都是查询，1%的请求是变更（除非是土豪，每秒钟都有收入在不断更改余额），所以，我们在这样的场景下，可
以加入缓存。

### 7.1 	Redis缓存与数据一致性的问题

那么基于上面的这个出发点，问题就来了，当用户的余额发生变化的时候，如何更新缓存中的数据，也就是说。

1. 我是先更新缓存中的数据再更新数据库的数据；
2. 还是修改数据库中的数据再更新缓存中的数据；

这就是我们经常会在面试遇到的问题，数据库的数据和缓存中的数据如何达到一致性？首先，可以肯定的是，
redis中的数据和数据库中的数据不可能保证事务性达到统一的，这个是毫无疑问的，所以在实际应用中，我们都
是基于当前的场景进行权衡降低出现不一致问题的出现概率。

#### 7.1.1 更新缓存还是让缓存失效

更新缓存表示数据不但会写入到数据库，还会同步更新缓存； 而让缓存失效是表示只更新数据库中的数据，然后删除缓存中对应的key。那么这两种方式怎么去选择？这块有一个衡量的指标。

1. 如果更新缓存的代价很小，那么可以先更新缓存，这个代价很小的意思是我不需要很复杂的计算去获得最新的
   余额数字。
2. 如果是更新缓存的代价很大，意味着需要通过多个接口调用和数据查询才能获得最新的结果，那么可以先淘汰
   缓存。淘汰缓存以后后续的请求如果在缓存中找不到，自然去数据库中检索。

#### 7.1.2 先操作数据库还是先操作缓存

当客户端发起事务类型请求时，假设我们以让缓存失效作为缓存的的处理方式，那么又会存在两个情况。

1. 先更新数据库再让缓存失效
2. 先让缓存失效，再更新数据库

前面我们讲过，更新数据库和更新缓存这两个操作，是无法保证原子性的，所以我们需要根据当前业务的场景的容
忍性来选择。也就是如果出现不一致的情况下，哪一种更新方式对业务的影响最小，就先执行影响最小的方案。

### 7.2 关于缓存雪崩的解决方案

当缓存大规模渗透在整个架构中以后，那么缓存本身的可用性讲决定整个架构的稳定性。那么接下来我们来讨论下
缓存在应用过程中可能会导致的问题。

#### 7.2.1 缓存雪崩

缓存雪崩是指设置缓存时采用了相同的过期时间，导致缓存在某一个时刻同时失效，或者缓存服务器宕机宕机导致
缓存全面失效，请求全部转发到了DB层面，DB由于瞬间压力增大而导致崩溃。缓存失效导致的雪崩效应对底层系
统的冲击是很大的。

解决方式：

1. 对缓存的访问，如果发现从缓存中取不到值，那么通过加锁或者队列的方式保证缓存的单进程操作，从而避免
   失效时并发请求全部落到底层的存储系统上；但是这种方式会带来性能上的损耗。
2. 将缓存失效的时间分散，降低每一个缓存过期时间的重复率。
3. 如果是因为缓存服务器故障导致的问题，一方面需要保证缓存服务器的高可用、另一方面，应用程序中可以采
   用多级缓存。

#### 7.2.2 缓存穿透

缓存穿透是指查询一个根本不存在的数据，缓存和数据源都不会命中。出于容错的考虑，如果从数据层查不到数据
则不写入缓存，即数据源返回值为 null 时，不缓存 null。缓存穿透问题可能会使后端数据源负载加大，由于很多后端数据源不具备高并发性，甚至可能造成后端数据源宕掉。

解决方式：

1. 如果查询数据库也为空，直接设置一个默认值存放到缓存，这样第二次到缓冲中获取就有值了，而不会继续访
   问数据库，这种办法最简单粗暴。比如，”key” , “&&”。在返回这个&&值的时候，我们的应用就可以认为这是不存在的key，那我们的应用就可以决定是否继续等待继续访问，还是放弃掉这次操作。如果继续等待访问，过一个时间轮询点后，再次请求这个key，如果取到的值不再是&&，则可以认为这时候key有值了，从而避免了透传到数据库，从而把大量的类似请求挡在了缓存之中。
2. 根据缓存数据Key的设计规则，将不符合规则的key进行过滤。采用布隆过滤器，将所有可能存在的数据哈希到一个足够大的BitSet中，不存在的数据将会被拦截掉，从而避免了对底层存储系统的查询压力。



## 8. BitMap

所谓的Bit-map就是用一个bit位来标记某个元素对应的Value， 而Key即是该元素。由于采用了Bit为单位来存储数据，因此在存储空间方面，可以大大节省。

BitMap可以进行数据的快速的查找，判重，删除，一般数据范围是int的10倍以下，也可以用来去重数据，从而达到数据的压缩。

### 8.1 BitMap排序示例

假设我们要对0-7(3,5,2,1,7,4,0,6)进行排序，我们可以采用BitMap。我们只要需8个Bit即可。

遍历这7个元素，第一个元素是5，就把5对应的位置置为1（p+(i/8)|(0×01<<(i%8)），依次操作即可。

| 字节位置       |  7   |  6   |  5   |  4   |  3   |  2   |  1   |  0   |
| -------------- | :--: | :--: | :--: | :--: | :--: | :--: | :--: | :--: |
| 存储数据的位置 |      |      |  1   |      |      |      |      |      |

### 8.2 BitMap排序复杂度分析

Bitmap排序需要的时间复杂度和空间复杂度依赖于数据中最大的数字。

bitmap排序的时间复杂度不是O(N)的，而是取决于待排序数组中的最大值MAX，在实际应用上关系也不大，比如我开10个线程去读byte数组，那么复杂度为:O(Max/10)。也就是要是读取的，可以用多线程的方式去读取。时间复杂度方面也是O(Max/n)，其中Max为byte[]数组的大小，n为线程大小。

空间复杂度应该就是O(Max/8)bytes。

### 8.3 BitMap优缺点

BitMap优点：

- 运算效率高，不进行比较和移位
- 占用内存少，比如最大的数MAX=10000000；只需占用内存为MAX/8=1250000Byte=1.25M

BitMap缺点：

- 所有的数据不能重复，即不可对重复的数据进行排序。（少量重复数据查找还是可以的，用2-bitmap）
- 当数据类似（1，1000，10万）只有3个数据的时候，用bitmap时间复杂度和空间复杂度相当大，只有当数据比较密集时才有优势。

### 8.4 BitMap应用

1 使用位图法判断整形数组是否存在重复
判断集合中存在重复是常见编程任务之一，当集合中数据量比较大时我们通常希望少进行几次扫描，这时双重循环法就不可取了。
位图法比较适合于这种情况，它的做法是按照集合中最大元素max创建一个长度为max+1的新数组，然后再次扫描原数组，遇到几就给新数组的第几位置上1，如遇到 5就给新数组的第六个元素置1，这样下次再遇到5想置位时发现新数组的第六个元素已经是1了，这说明这次的数据肯定和以前的数据存在着重复。这种给新数组初始化时置零其后置一的做法类似于位图的处理方法故称位图法。它的运算次数最坏的情况为2N。如果已知数组的最大值即能事先给新数组定长的话效率还能提高一倍。

2 在2.5亿个整数中找出不重复的整数，注，内存不足以容纳这2.5亿个整数

将bit-map扩展一下，采用2-Bitmap（每个数分配2bit，00表示不存在，01表示出现一次，10表示多次，11无意义）进行，共需内存2^32 * 2 bit=1 GB内存，还可以接受。然后扫描这2.5亿个整数，查看Bitmap中相对应位，如果是00变01，01变10，10保持不变。所描完事后，查看bitmap，把对应位是01的整数输出即可。

3  已知某个文件内包含一些电话号码，每个号码为8位数字，统计不同号码的个数。

8位最多99 999 999，大概需要99m个bit，大概10几m字节的内存即可。 （可以理解为从0-99 999 999的数字，每个数字对应一个Bit位，所以只需要99M个Bit==1.2MBytes，这样，就用了小小的1.2M左右的内存表示了所有的8位数的电话）。

4  给40亿个不重复的unsigned int的整数，没排过序的，然后再给一个数，如何快速判断这个数是否在那40亿个数当中？

解析:bitmap算法就好办多了。申请512M的内存，一个bit位代表一个unsigned int值，读入40亿个数，设置相应的bit位；读入要查询的数，查看相应bit位是否为1，为1表示存在，为0表示不存在。
Note: unsigned int最大数为2^32 - 1，所以需要2^32 - 1个位，也就是(2^32 - 1) / 8 /10 ^ 9G = 0.5G内存。

逆向思维优化：usinged int只有接近43亿（unsigned int最大值为232-1=4294967295,最大不超过43亿），所以可以用某种方式存没有出现过的3亿个数（使用数组{大小为3亿中最大的数/8 bytes}存储），如果出现在3亿个数里面，说明不在40亿里面。3亿个数存储空间一般小于40亿个。（xx存储4294967296需要512MB， 存储294967296只需要35.16MBxx）。

5 给定一个数组a，求所有和为SUM的两个数。

如果数组都是整数（负数也可以，将所有数据加上最小的负数x，SUM += 2x就可以了）。如a = [1,2,3,4,7,8]，先求a的补数组[8,7,6,5,2,1]，开辟两个数组b1,b2（最大数组长度为SUM/8/2{因为两数满足和为SUM，一个数<SUM/2，另一个数也就知道了}，这样每个b数组最大内存为SUM/(8*2*1024*1024) = 128M）,使用bitmap算法和数组a分别设置b1b2对应的位为1，b1b2相与就可以得到和为SUM的两个数其中一个数了。



## 9. 布隆过滤器

### 9.1 Bloom过滤器的简介

布隆过滤器是Burton Howard Bloom在1970年提出来的，一种空间效率极高的概率型算法和数据结构，主要用来
判断一个元素是否在集合中存在。因为他是一个概率型的算法，所以会存在一定的误差，如果传入一个值去布隆过
滤器中检索，可能会出现检测存在的结果但是实际上可能是不存在的，但是肯定不会出现实际上不存在然后反馈存
在的结果。因此，Bloom Filter不适合那些“零错误”的应用场合。而在能容忍低错误率的应用场合下，Bloom Filter
通过极少的错误换取了存储空间的极大节省。

布隆过滤器的原理是，当一个元素被加入集合时，通过K个散列函数将这个元素映射成一个位数组中的K个点，把它们置为1。检索时，我们只要看看这些点是不是都是1就（大约）知道集合中有没有它了：如果这些点有任何一个0，则被检元素一定不在；如果都是1，则被检元素很可能在。

Bloom Filter的这种高效是有一定代价的：在判断一个元素是否属于某个集合时，有可能会把不属于这个集合的元素误认为属于这个集合（false positive）。因此，Bloom Filter不适合那些“零错误”的应用场合。而在能容忍低错误率的应用场合下，Bloom Filter通过极少的错误换取了存储空间的极大节省。BF其中的元素越多，false positive rate(误报率)越大，但是false negative (漏报)是不可能的。

### 9.2 Bloom过滤器的应用

- 可以用来实现数据字典，进行数据的判重，或者集合求交集。在垃圾邮件过滤的黑白名单方法、爬虫(Crawler)的网址判重模块中等等经常被用到。
- 用于拼写检查和数据库系统中。
- ...

### 9.3 Bloom过滤器的优缺点

#### 9.3.1 优点

1. 相比于其它的数据结构，布隆过滤器在空间和时间方面都有巨大的优势。布隆过滤器存储空间和插入/查询时间都是常数（O(k)）。哈希表也能用于判断元素是否在集合中，但是布隆过滤器只需要哈希表的1/8或1/4的空间复杂度就能完成同样的问题。

   如果可能元素范围不是很大，并且大多数都在集合中，则使用确定性的bit array远远胜过使用布隆过滤器。因为bit array对于每个可能的元素空间上只需要1 bit，add和query的时间复杂度只有O(1)。注意到这样一个哈希表（bit array）只有在忽略collision并且只存储元素是否在其中的二进制信息时，才会获得空间和时间上的优势，而在此情况下，它就有效地称为了k=1的布隆过滤器。

   而当考虑到collision时，对于有m个slot的bit array或者其他哈希表（即k=1的布隆过滤器），如果想要保证1%的误判率，则这个bit array只能存储m/100个元素，因而有大量的空间被浪费，同时也会使得空间复杂度急剧上升，这显然不是space efficient的。解决的方法很简单，使用k>1的布隆过滤器，即k个hash function将每个元素改为对应于k个bits，因为误判度会降低很多，并且如果参数k和m选取得好，一半的m可被置为为1，这充分说明了布隆过滤器的space efficient性。

2. 散列函数相互之间没有关系，方便由硬件并行实现。

3. 布隆过滤器不需要存储元素本身，在某些对保密要求非常严格的场合有优势。

4. 布隆过滤器可以表示全集，其它任何数据结构都不能。

5. k和m相同，使用同一组散列函数的两个布隆过滤器的交并差运算可以使用位操作进行。

#### 9.3.2 缺点

1. 当k很大时，设计k个独立的hash function是不现实并且困难的。对于一个输出范围很大的hash function（例如MD5产生的128 bits数），如果不同bit位的相关性很小，则可把此输出分割为k份。或者可将k个不同的初始值（例如0,1,2, … ,k-1）结合元素，feed给一个hash function从而产生k个不同的数。
2. Counting bloom filter（CBF）将位数组中的每一位扩展为一个counter，从而支持了元素的删除操作。BF不支持删除一个已经插入的关键字，因为该关键字对应的位会牵动到其他的关键字。所以用一个counter数组代替位数组，就可以支持删除了。 
3. Spectral Bloom Filter（SBF）将其与集合元素的出现次数关联。SBF采用counter中的最小值来近似表示元素的出现频率。应该就是[Count-Min Sketch 算法]。












