# 虚拟机centos7使用主机代理的简单配置

## 1 配置全局代理

```shell
#打开profile文件
vim /etc/profile

#添加Proxy代理信息（其中username和password根据需要填写，若无则不填写, proxy_ip 为主机ip, port为主机代理开放的端口）
http_proxy=http://username:password@proxy_ip:port/
https_proxy=http://username:password@proxy_ip:port/
ftp_proxy=http://username:password@proxy_ip:port/
export http_proxy
export https_proxy
export ftp_proxy

#生效配置
source /etc/profile
```

## 2 配置yum代理

```shell
#打开yum.conf文件
vim /etc/yum.conf

#添加Proxy代理信息
proxy=http://username:password@proxy_ip:port/
```

## 3 配置wget代理

```shell
#打开/wgetrc文件
vim /etc/wgetrc

#修改Proxy代理信息
http_proxy=http://username:password@proxy_ip:port/
ftp_proxy=http://username:password@proxy_ip:port/
```

