# MongoDB学习

## 1. MongoDB应用场景及实现原理

### 1.1 MongoDB应用场景

a. 网站的实时数据：例如：日志、timeline、用户行为

b. 数据缓存

c. 存储大尺寸、低价值的数据：搜索引擎的图片文件、视频文件等

d. 高伸缩性的场景

e. 对象或JSON数据

### 1.2 MongoDB不适用场景

a. 高度事务性系统：例如金融系统的核心数据，高机密的用户数据

b. 传统的商业智能化应用：例如关联查询较多的情况以及复杂的查询

### 1.3 MongoDB 4.0 支持事务操作

### 1.4 MongoDB底层实现原理（集群部署情况下的三种角色）

实际数据存储结点、配置文件存储结点、路由接入结点

连接的客户端直接与路由结点相连，从配置结点上查询数据，根据查询结果到实际的存储结点上查询和
存储数据



## 2. MongoDB常用命令及配置

### 2.1 常用命令

1. 创建数据库

   use DBName

2. 创建集合

   db.collectionName.insert({key:value})

3. 查询

   db.collectionName.find(condition)

   db.collectionName.findOne(condition)

4. 修改

   不会影响其他属性列，主键冲突会报错

   db.collectionName.update({condition},{$set{key,value}})

   第三个参数为true 则执行insertOrUpdate 操作，查询出则更新，没查出则插入

   db.collectionName.update({condition},{$set{key,value}},true)

5. 删除

   删除满足条件的第一条只删除数据不删除索引

   db.collectionName.remove({condition})

   删除集合

   db.collectionName.drop()

   删除数据库

   db.dropDatabase()

6. 查看集合

   show collections

7. 查看数据库

   show dbs

8. 插入数据

   不允许键值重复

   db.collectionName.insert()

   若键值重复，可改为替换操作

   db.collectionName.save()

9. 批量更新

   批量操作需要和选择器同时使用，第一个false 表示不执行insertOrUpdate 操作，第二个true 表示执行批量

   db.collectionName.update({condition},{$set:{key:value}},false,true)

10. $set

    指定一个键值对，若存在就进行修改，不存在则添加

11. $inc ：只使用于数字类型，可以为指定键值对的数字类型进行加减操作

    db.t_member.update({name:"zhangsan"},{$inc:{age:2}})

12. $unset : 删除指定的键

    db.t_member.update({name:"zhangsan"},{$unset:{age:1}})

13. $push : 数组键操作：1、如果存在指定的数组，则为其添加值；2、如果不存在指定的数组，则创建数组键，并添加值；3、如果指定的键不为数组类型，则报错；

14. $addToSet : 当指定的数组中有这个值时，不插入，反之插入

    db.t_member.update({name:"zhangsan"},{$addToSet:{classes:"English"}})

15. $pop：删除指定数组的值，当value=1 删除最后一个值，当value=-1 删除第一个值

    db.t_member.update({name:"zhangsan"},{$pop:{classes:1}})

16. $pull : 删除指定数组指定的值

    db.persons.update({name:"zhangsan"},{$pull:{classes:"Chinese"}})

17. $pullAll 批量删除指定数组

18. $ : 修改指定数组时，若数组有多个对象，但只想修改其中一些，则需要定位器

    db.t_member.update({"classes.type":"AA"},{$set:{"classes.$.sex":"male"}})

19. $addToSet 与$each 结合完成批量数组更新操作

    db.t_member.update({name:"zhangsan"},{$set:{classes:{$each:["chinese","art"]}}})

20. runCommand 函数和findAndModify 函数

    runCommand({
    findAndModify:"persons",
    query:{查询器},
    sort:{排序},
    update:{修改器},
    new:true 是否返回修改后的数据
    });

21. 高级查询

    1. 第一个空括号表示查询全部数据，第二个括号中值为0 表示不返回，值为1 表示返回，默认情况下若不指定主键，主键总是会被返回

       db.t_member.find({},{_id:0,name:1})

    2. 比较操作符：$lt: < $lte: <= $gt: > $gte: >= $ne: !=

       db.persons.find({条件},{指定键});

       查询年龄大于等于25 小于等于27 的人

       db.t_member.find({age:{$gte:25,$lte:27}},{_id:0,name:1,age:1})

    3. 包含与不包含（仅针对于数组） $in 或$nin

       查询国籍是中国或美国的学生信息
       db.t_member.find({country:{$in:["China","USA"]}},{_id:0,name:1:country:1})

    4. $or 查询

       查询语文成绩大于85 或者英语大于90 的学生信息

       db.t_member.find({$or:[{c:{$gt:85}},{e:{$gt:90}}]},{_id:0,name:1,c:1,e:1})

    5. 正则表达式

       查询出名字中存在”li”的学生的信息

       db.t_member.find({name:/li/i},{_id:0,name:1})

    6. $not 的使用

       $not 和$nin 的区别是$not 可以用在任何地方儿$nin 是用到集合上的

       查询出名字中不存在”li”的学生的信息
       db.t_member.find({name:{$not:/li/i}},{_id:0,name:1})

    7. $all 与index 的使用

       查询喜欢看MONGOD 和JS 的学生

       db.t_member.find({books:{$all:["JS","MONGODB"]}},{_id:0,name:1})

       查询第二本书是JAVA 的学习信息

       db.t_member.find({"books.1":"JAVA"},{_id:0,name:1,books:1})

    8. $size 的使用，不能与比较查询符同时使用

       查询出喜欢的书籍数量是4 本的学生

       db.t_member.find({books:{$size:4}},{_id:0,name:1})

    9. $slice 操作符返回文档中指定数组的内部值

       查询出Jim 书架中第2~4 本书

       db.t_member.find({name:"jim"},{_id:0,name:1,books:{$slice:[1,3]}})

    10. $elemMatch

        查询出在K 上过学且成绩为A 的学生

        db.t_member.find({school:{$elemMatch:{school:"K",score:"A"}},{_id:0,name:1})

    11. 分页与排序

        limit 返回指定条数查询出persons 文档中前5 条数据

        db.t_member.find({},{_id:0,name:1}).limit(5)

        指定数据跨度查询出persons 文档中第3 条数据后的5 条数据

        db.t_member.find({},{_id:0,name:1}).limit(5).skip(3)

        sort 排序1 为正序，-1 为倒序

        db.t_member.find({},{_id:0,name:1,age:1}).limit(5).skip(3).sort({age:1})

        mongodb 的key 可以存不同类型的数据排序就也有优先级

        最小值->null->数字->字符串->对象/文档->数组->二进制->对象ID->布尔->日期->时间戳->正则->最大值

    12. 游标

        var persons = db.persons.find();
        while(persons.hasNext()){
        obj = persons.next();
        print(obj.name)
        }

        游标几个销毁条件:
        a.客户端发来信息叫他销毁
        b.游标迭代完毕
        c.默认游标超过10 分钟没用也会别清除

    13. 查询快照

        快照后就会针对不变的集合进行游标运动了

        用快照则需要用高级查询

        db.persons.find({$query:{name:”Jim”},$snapshot:true})

        高级查询选项
        1)$query
        2)$orderby
        3)$maxsan：integer 最多扫描的文档数
        4)$min：doc 查询开始
        5)$max：doc 查询结束
        6)$hint：doc 使用哪个索引
        7)$explain:boolean 统计
        8)$snapshot:boolean 一致快照

    14.  查询点

        查询点(70,180)最近的3 个点

        db.map.find({gis:{$near:[70,180]}},{_id:0,gis:1}).limit(3)

        查询以点(50,50)和点(190,190)为对角线的正方形中的所有的点

        db.map.find({gis:{$within:{$box:[[50,50],[190,190]]}}},{_id:0,gis:1})

        查询出以圆心为(56,80)半径为50 规则下的圆心面积中的点

        db.map.find({gis:{$with:{$center:[[56,80],50]}}},{_id:0,gis:1})

    15. Count+Distinct+Group

        15.1 count 查询结果条数

        db.persons.find({country:"USA"}).count()

        15.2 Distinct 去重

        请查询出persons 中一共有多少个国家分别是什么,key 表示去重的键

        db.runCommand({distinct:"persons",key:"country"}).values

        15.3 group 分组

        分组首先会按照key 进行分组,每组的每一个文档全要执行$reduce 的方法,他接收2 个参数一个是组内本条记录,一个是累加器数据.

        db.runCommand({ group:{
        ns:"集合的名字",
        key:"分组键对象",
        initial:"初始化累加器",
        $reduce:"分解器",
        condition:"条件",
        finalize:"组完成器"
        }})

        请查出persons 中每个国家学生数学成绩最好的学生信息(必须在90 以上)

        db.runCommand({
        	group:{
        		ns:"persons",
        		key:{"country":true},
        		initial:{m:0},
        		$reduce:function(doc,prev){
        			if(doc.m>prev.m){
        				prev.m = doc.m;
        				prev.name = doc.m;
        				prev.country = doc.country;
        			}
        		},

        		condition:{m:{$gt:90}},
        		finalize:function(prev){
        			prev.m = prev.name+" comes from "+prev.country+" ,Math score is"+prev.m;
        		}
        	}
        })

        15.4 函数格式化分组键

        如果集合中出现键Counrty 和counTry 同时存在

        $keyf:function(doc){
        if(doc.country){
        return {country:doc.country}
        }
        return {country:doc.counTry}
        }

    16. 常用命令举例

        16.1 查询服务器版本号和主机操作系统

        db.runCommand({buildInfo:1})

        16.2 查询执行集合的详细信息,大小,空间,索引等

        db.runCommand({collStats:"persons"})

        16.3 查看操作本集合最后一次错误信息

        db.runCommand({getLastError:"persons"})

    17. 固定集合

        17.1 特性

        固定集合默认是没有索引的就算是_id 也是没有索引的,由于不需分配新的空间他的插入速度是非常快的,固定集合的顺是确定的导致查询速度是非常快的,最适合就是日志管理.

        17.2 创建固定集合

        创建一个新的固定集合要求大小是100 个字节,可以存储文档10 个

        db.createCollection("mycoll",{size:100,capped:true,max:10})

        把一个普通集合转换成固定集合

        db.runCommand({convertToCapped:"persons",size:1000})

        对固定集合反向排序，默认情况是插入的顺序排序

        db.mycoll.find().sort({$natural:-1})
